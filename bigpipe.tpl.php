<script type="text/javascript">
  // If we are outside of the BigPipe frame, redirect to the loaded page
  if(!window.parent.Drupal) {
    location.href = '<?php print base_path() . $_GET['q']; ?>';
  }
  // Reference parent's variables
  var parent = window.parent;
  var $ = parent.$;
  var Drupal = parent.Drupal;
  var History = parent.History;
  // Update head title
  parent.document.title = '<?php print $head_title; ?>';
  // Replace page regions
  <?php foreach(bigpipe_regions() as $region => $id): ?>
    Drupal.BigPipe.regions.push(
      $('#<?php print $id ?>')
        .replaceWith(<?php print json_encode(theme($region, get_defined_vars())); ?>)
        .selector
    );
  <?php endforeach; ?>
  Drupal.BigPipe.addScripts(<?php print json_encode(str_replace("\n", '', $scripts)); ?>);
  Drupal.BigPipe.addStyles(<?php print json_encode(str_replace("\n", '', $styles)); ?>);
  // Update the browser's url
  History.pushState(null, null, '<?php print base_path() . $_GET['q']; ?>');
  // Clean the frame to free memory
  document.write();
</script>