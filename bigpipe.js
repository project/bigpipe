(function($) {

Drupal.BigPipe = {
  scripts: [],
  inline: [],
  regions: []
};

/**
 * Add scripts to the loading queue.
 */
Drupal.BigPipe.addScripts = function(scripts) {
  $(scripts).each(function() {
	var src = $(this).attr('src');
	// Get rid of jquery.js since its already loaded to avoid jquery_update conflicts
	if(src && src.indexOf('jquery.js') == -1) {
      if(!$('script[src*=' + src + ']').size()) {
    	Drupal.BigPipe.scripts.push(this);
      }
	}
	// If src is not present, then we have an inline script
	else {
      Drupal.BigPipe.inline.push(this);
	}
  });
  // Start the load thread
  Drupal.BigPipe.loadScripts();
}

/**
 * Embed styles to the page.
 */
Drupal.BigPipe.addStyles = function(styles) {
  var position = $('link:last');
  $(styles).each(function() {
	var href = $(this).attr('href');
	if(href) {
      if($('link[href*=' + href + ']').size()) {
    	// Grab the current position
        position = 'link[href*=' + href + ']';
      }
      else {
    	// Put the script in the right place
    	$(this).insertAfter(position);
      }
	}
  });
}

/**
 * Load the scripts one by one, until the previous one is loaded.
 */
Drupal.BigPipe.loadScripts = function() {
  if(Drupal.BigPipe.scripts.length) {
	// Obtain and remove one script from the queue
	var script = Drupal.BigPipe.scripts.shift();
	src = script.src;
    $.getScript(src, function() {
    	Drupal.BigPipe.loadScripts();
    });
  }
  else {
	// Execute inline scripts loaded
    $(Drupal.BigPipe.inline).each(function() {
      // Evaluate cleaning the code
      eval(this.innerHTML.replace('<!--//--><![CDATA[//><!--', '').replace('//--><!]]>', ''));
    });
    // Reset the inline scripts to zero
    Drupal.BigPipe.inline = [];
    // Attach behaviors to all the regions loaded
    $(Drupal.BigPipe.regions).each(function(i, region) {
      Drupal.attachBehaviors($(region));
    });
    // Reset the regions to zero
    Drupal.BigPipe.regions = [];
  }
}

})(jQuery);

/**
 * Create the BigPipe's frame, this must be done just once.
 */
$(document).ready(function() {
  $('<iframe id="bigpipe" name="bigpipe" style="display:none;" />')
    .appendTo('body');
});

/**
 * Attach BigPipe functionality to links and forms.
 */
Drupal.behaviors.bigpipe = function(context) {
  // Open external links in new window
  $('a[href^="http"]', context).attr('target','_blank');
  // Process internal links with BigPipe, we use 'live' to leave intact functionalities such as ajax
  $('a:not([href^="http"])', context).live('click', function(e) {
    var href = $(this).attr('href');
    if (!href.match(/^#/) && e.ctrlKey == false) {
      // Add the bigpipe parameter
      $('iframe[name="bigpipe"]').attr('src', href + ((href.indexOf('?') == -1) ? '?' : '&') + 'bigpipe');
      return false;
    }
  });
  // Process forms with bigpipe
  $('form', context).each(function() {
    $(this)
      .append('<input type="hidden" name="bigpipe" value="1" />')
      .attr('target', 'bigpipe');
  });
}